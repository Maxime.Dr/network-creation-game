import io.jbotsim.core.Node;
import io.jbotsim.core.Link;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.LinkedList;
import java.util.Random;

public class Agent extends Node{

    public static int id = 0;
    public final int agent_id;
    private int number_change;
    private LinkedList<Integer> strategy;
    private LinkedList<Link> link_used;
    private LinkedList<Link> link_bought; //trie par ordre decroissant
    private double weight = 0.0;

    public Agent(LinkedList<Integer> strat) {
        this.agent_id = id++;
        this.number_change = 0;
        this.strategy = strat;
        this.link_used = new LinkedList<Link>();
        this.link_bought = new LinkedList<Link>();
    }

    /* ############################# CHECK STRATEGY ##################################### */

    /**
     * Method to determinate the list of Agents we can reach, this method
     * looks like to the algorithm Depth First Search. Indeed, we store the
     * nodes/Agents visited and we check recursively with our neighbors if they
     * are not in the visited nodes.
     *
     * @param connected is a list of visited nodes
     * @return a LinkedList<Integer>
     */
    private LinkedList<Integer> getConnectedAgent(LinkedList<Integer> connected){
        LinkedList<Integer> connected_agents = connected;
        connected_agents.add(this.agent_id);

        /* ADDING MY NEIGHBORS DIRECT */
        for(Link l : this.link_used){
            Agent src = (Agent) l.source;
            Agent dst = (Agent) l.destination;

            /* IF THE SOURCE AGENT IS I AND I DON'T SAW THE AGENT DST */
            if(src.agent_id == this.agent_id && connected_agents.contains(dst.agent_id) == false){
                LinkedList<Integer> res = dst.getConnectedAgent(connected_agents);
                for(int i : res){
                    if(connected_agents.contains(i) == false){
                        connected_agents.addLast(i);
                    }
                }
            }

            /* I AM THE DESTINATION AGENT AND I DON'T SAW THE AGENT SRC */
            if(src.agent_id != this.agent_id && connected_agents.contains(src.agent_id) == false){
                LinkedList<Integer> res = src.getConnectedAgent(connected_agents);
                for(int i : res){
                    if(connected_agents.contains(i) == false){
                        connected_agents.addLast(i);
                    }
                }
            }
        }

        return  connected_agents;
    }

    /**
     * Method to determinate if the Agent respect his strategy.
     * Indeed, we verify if the Agent is connected with each Agent
     * of his strategy.
     *
     * @return a boolean
     */
    public boolean strategyRespected() {

        /* GET THE AGENTS WE CAN REACH */
        LinkedList<Integer> connected_agents = getConnectedAgent(new LinkedList<Integer>());

        /* CHECKING IF THE AGENT RESPECTS HIS STRATEGY */
        for(int i : this.strategy){
            if(this.agent_id != i && connected_agents.contains(i) == false){
                return false;
            }
        }
        return true;
    }

    /* ############################# MANAGE DECISION ##################################### */

    /**
     * Method to create a list of links available sorted in ascending order
     * or in descending order in accordance of the value of STATUS for the Agent.
     * In a first time, we get the neighbors of the Agent, then we search a minimal/maximal.
     * When we found a minimal/maximal, we store its index then we create a link in order to add this link
     * to the list. Then we update the value of this index to infinity
     * (in order to this index would never be choosing again as minimal/maximal)
     *
     * @param matrix
     * @param agents
     * @return LinkedList<Link>
     */
    public LinkedList<Link> getLinksOrder(double[][] matrix, LinkedList<Agent> agents){
        LinkedList<Link> links = new LinkedList<>();
        int length = matrix.length -1;
        double[] cost_neighbors = matrix[this.agent_id].clone();

        /* CREATION OF THE LIST SORTED IN ASCENDING ORDER */
        if(Simulator.STATUS.compareTo("COST") == 0 || Simulator.STATUS.compareTo("TIME") == 0){

            double minimal = Double.POSITIVE_INFINITY;
            int index = -1;
            for(int i=0; i<length; i++){

                /* FIND A MINIMAL */
                for(int j=0; j<=length; j++){
                    if(this.agent_id != j && cost_neighbors[j] < minimal){
                        minimal = cost_neighbors[j];
                        index = j;
                    }
                }

                /* ADDING THE NEW LINK */
                Link l = new Link(this, agents.get(index));
                Agent src = (Agent) l.source;
                Agent dst = (Agent) l.destination;

                links.addLast(l);

                /* UPDATE THE ARRAY */
                cost_neighbors[index] = Double.POSITIVE_INFINITY;
                index = -1;
                minimal = Double.POSITIVE_INFINITY;
            }
        }

        /* CREATION OF THE LIST SORTED IN DESCENDING ORDER */
        else{

            double maximal = Double.NEGATIVE_INFINITY;
            int index = -1;
            for(int i=0; i<length; i++){

                /* FIND A MAXIMAL */
                for(int j=0; j<=length; j++){
                    if(this.agent_id != j && cost_neighbors[j] > maximal){
                        maximal = cost_neighbors[j];
                        index = j;
                    }
                }

                /* ADDING THE NEW LINK */
                Link l = new Link(this, agents.get(index));
                Agent src = (Agent) l.source;
                Agent dst = (Agent) l.destination;

                links.addLast(l);

                /* UPDATE THE ARRAY */
                cost_neighbors[index] = Double.NEGATIVE_INFINITY;
                index = -1;
                maximal = Double.NEGATIVE_INFINITY;
            }
        }

        return links;
    }


    /**
     * Method to decide if the Agent can give back a link (remove this link)
     * without compromise his strategy
     *
     * @return Link
     */

    public Link decisionToRemove(){
        for(Link l : this.link_bought){
            Agent dst = (Agent) l.destination;
            this.link_used.remove(l);
            dst.link_used.remove(l);

            /* CAN GIVE BACK THIS LINK WITHOUT TO COMPROMISE HIS STRATEGY */
            if(this.strategyRespected() == true){
                this.link_used.add(l);
                dst.link_used.add(l);
                return l;
            }
            this.link_used.add(l);
            dst.link_used.add(l);
        }
        return null;
    }

    /*
        - creer une liste des liens disponible (= ceux qui ne sont pas bought et used) (dans un ordre croissant)
        - acheter le premier elemebt de la liste
     */
    public Link decisionToAdd(double[][] matrix, LinkedList<Agent> agents){
        LinkedList<Link> links_available = getLinksOrder(matrix,agents);
        LinkedList<Integer> index_delete = new LinkedList<>();

        /* IN ORDER TO REMOVE LINKS USED FROM AVAILABLE LINKS */
        for(int i=0; i<links_available.size(); i++){
            if(index_delete.size() == this.link_used.size()){
                break;
            }
            Link tmp_available = links_available.get(i);
            for(int j=0; j<this.link_used.size(); j++){
                Link tmp_used = this.link_used.get(j);

                /* IF WE FIND A LINK PRESENT IN THE USED LINK --> STORE ITS INDEX */
                if((tmp_available.source.getID() == tmp_used.source.getID() || tmp_available.source.getID() == tmp_used.destination.getID()) &&
                        (tmp_available.destination.getID() == tmp_used.source.getID() || tmp_available.destination.getID() == tmp_used.destination.getID())){
                    index_delete.addFirst(i);
                    break;
                }
            }
        }

        /* REMOVE LINKS FROM AVAILABLE LINKS */
        for(int i=0; i<index_delete.size(); i++){
            links_available.remove((int) index_delete.get(i));
        }
        if(links_available.isEmpty() == false){
            return links_available.remove(0);
        }
        else{
            return null;
        }
    }

    /* ############################# MANAGE PLAY ##################################### */

    /**
     * Method to play, precisely allow the Agent to make a decision.
     * Indeed,  if the agent respect its strategy then we verify if
     * he can remove a link. In the other case, if we cannot remove
     * the link then the Agent choose to buy a link or do nothing based
     * on the value of selfish. In the case where the strategy is not respected
     * then the Agent choose to buy a link or do nothing based on the value
     * of selfish too
     *
     * @param agents
     * @param matrix
     * @param selfish
     */
    public void play(LinkedList<Agent> agents, double[][] matrix, double selfish){

        Simulator.writing(Simulator.FILENAME, "the node " + this.agent_id + " plays : ");
        Link l_add = this.decisionToAdd(matrix,agents);

        /* IF THE AGENT RESPECTS HIS STRATEGY */
        if(this.strategyRespected()){
            Link l = this.decisionToRemove();

            /* CANNOT GIVE BACK A LINK */
            if(l == null){

                Random r = new Random();
                double random = r.nextDouble();

                /* AGENT BUY A LINK */
                if(random > selfish && l_add != null) {

                    this.getTopology().addLink(l_add);
                    //this.link_used.add(l_add);
                    this.addLink(l_add);
                    //this.link_bought.add(l_add);//false we must insert l_add dans un certain ordre
                    double price = matrix[l_add.source.getID()][l_add.destination.getID()];
                    this.buyLink(l_add,price,matrix);
                    this.updateWeight(matrix);

                    Agent desti = (Agent) l_add.destination;
                    desti.link_used.add(l_add);

                    Simulator.writing(Simulator.FILENAME, "buy a link : " + this + "<-->" + desti + "\n");
                }

                /* AGENT DO NOTHING */
                else{
                    Simulator.writing(Simulator.FILENAME, "do nothing\n");
                }
            }

            /* CAN GIVE BACK A LINK */
            else{

                /* UPDATE TOPOLOGY AND THE AGENT */
                this.getTopology().removeLink(l); // remove this link from topology
                this.link_bought.remove(l); // remove this link from bought links by the agent
                this.link_used.remove(l); // remove this link from used links by the agent
                this.updateWeight(matrix); // update the weight of the agent todo --> explain more
                this.number_change += 1; // the agent make a changement in the topology

                /* UPDATE THE CONNECTED AGENT */
                Agent desti = (Agent) l.destination;
                desti.link_used.remove(l);

                //System.out.println("Remove link : (" +l.source.getID()+ " ; "+ l.destination.getID()+")");
                Simulator.writing(Simulator.FILENAME, "decide to give back the link : " + this + " <--> " + desti + "\n");
            }
        }

        /* THE STRATEGY IS NOT RESPECTED --> MUST ADD A LINK */
        else{

            Random r = new Random();
            double random = r.nextDouble();

            /* AGENT BUY A LINK */
            if(random > selfish && l_add != null) {
                this.getTopology().addLink(l_add);
                this.addLink(l_add);
                double price = matrix[l_add.source.getID()][l_add.destination.getID()];
                this.buyLink(l_add,price,matrix);
                this.updateWeight(matrix);

                Agent desti = (Agent) l_add.destination;
                desti.link_used.add(l_add);

                //System.out.println("Add link : (" +l_add.source.getID()+ " ; "+ l_add.destination.getID()+")");
                Simulator.writing(Simulator.FILENAME, "buy a link : " + this + "<-->" + desti + "\n");
            }

            /* AGENT DO NOTHING */
            else{
                Simulator.writing(Simulator.FILENAME, "do nothing\n");
            }
        }
    }

    /* ############################# MANAGE LINKS ##################################### */

    /**
     * Method to add a link to the link_used
     *
     * @param l a link
     */
    public void addLink(Link l) {
        this.link_used.add(l);
    }

    /**
     * Method to add a link to link_bought in an ascending or
     * descending order in terms of STATUS
     *
     * @param l a link we have to add
     * @param price the cost of the link
     * @param matrix is the matrix of cost of links
     */
    public void buyLink(Link l, double price, double[][] matrix){
        int index = 0;

        /* ASCENDING ORDER */
        if(Simulator.STATUS.compareTo("COST") == 0 || Simulator.STATUS.compareTo("TIME") == 0){
            for(int i=0; i<this.link_bought.size(); i++){
                Link ele = this.link_bought.get(i);
                Agent n1 = (Agent) ele.source;
                Agent n2 = (Agent) ele.destination;
                double cost = matrix[n1.agent_id][n2.agent_id];
                if(cost > price){
                    index = i;
                    i = this.link_bought.size();
                }
            }
        }

        /* DESCENDING ORDER */
        else{
            for(int i=0; i<this.link_bought.size(); i++){
                Link ele = this.link_bought.get(i);
                Agent n1 = (Agent) ele.source;
                Agent n2 = (Agent) ele.destination;
                double cost = matrix[n1.agent_id][n2.agent_id];
                if(cost < price){
                    index = i;
                    i = this.link_bought.size();
                }
            }
        }

        this.link_bought.add(index, l);
    }

    /*
    function to update the weight of the Agent, there are 2 modes :
    - sum if mode is true :
    - mult else :
    */
    public void updateWeight(double[][] matrix){
        double w = 0.0;
        for(Link l : this.link_bought){
            Agent n1 = (Agent) l.source;
            Agent n2 = (Agent) l.destination;
            double cost = matrix[n1.agent_id][n2.agent_id];

            /* CALCULATE THE COST OR TIME */
            if(Simulator.STATUS.compareTo("COST") == 0 || Simulator.STATUS.compareTo("TIME") == 0){
                w += cost;
            }

            /* CALCULATE THE EFFICIENT */
            else{
                w *= cost;
            }
        }

        this.weight = w;
    }

    public String toString(){
        String s = "Agent " + this.agent_id;
        return s;
    }

    public void showLinks(){
        System.out.println("Link bought");
        for(Link l : this.link_bought){
            Agent src = (Agent) l.source;
            Agent dst = (Agent) l.destination;
            System.out.println(src.agent_id + " <--> " + dst.agent_id);
        }
        System.out.println("Link used");
        for(Link l : this.link_used){
            Agent src = (Agent) l.source;
            Agent dst = (Agent) l.destination;
            System.out.println(src.agent_id + " <--> " + dst.agent_id);
        }
    }

    /* ########################## DISPLAY LOGFILE ################################ */

    /**
     * Method to display the strategy of the Agent. This method
     * is called in order to display the strategy of each Agent
     * into the logfile
     * @return
     */
    public String display_strategy_logfile(){
        String s = "";
        for(int i=0; i<this.strategy.size(); i++){
            s += "Agent " + this.strategy.get(i);
            if(i+1 < this.strategy.size()){
                s += "; ";
            }
        }
        return s;
    }

    /* ########################## DISPLAY JSON ################################ */

    /**
     * Method to create a JSONObject that represent the
     * information about the Agent.
     *
     * @return JSONObject
     */
    public JSONObject Agent_to_JSON(){

        /* CREATE JSON OBJECT */
        JSONObject obj = new JSONObject();

        /* ADD INFORMATION */
        obj.put("id", this.agent_id);
        obj.put("number_changes", this.number_change);
        obj.put("weight", this.weight);

        /* ADD STRATEGY */
        JSONArray strat = new JSONArray();
        for(int ele : this.strategy){
            strat.add(ele);
        }
        obj.put("strategy", strat);

        /* ADD LINKS BOUGHT */
        JSONArray link_bought = new JSONArray();
        for(Link l : this.link_bought){
            Agent src = (Agent) l.source;
            Agent dst = (Agent) l.destination;
            if(src.agent_id == this.agent_id){
                link_bought.add(dst.agent_id);
            }
            else{
                link_bought.add(src.agent_id);
            }
        }
        obj.put("links_bought", link_bought);

        /* ADD LINKS USED */
        JSONArray link_used = new JSONArray();
        for(Link l : this.link_used){
            Agent src = (Agent) l.source;
            Agent dst = (Agent) l.destination;
            if(src.agent_id == this.agent_id){
                link_used.add(dst.agent_id);
            }
            else{
                link_used.add(src.agent_id);
            }
        }
        obj.put("links_used", link_used);

        return obj;
    }
}