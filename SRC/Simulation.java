import io.jbotsim.contrib.algos.Algorithms;
import io.jbotsim.core.Topology;
import io.jbotsim.ui.JViewer;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.LinkedList;

public class Simulation {

    public final int limit;
    private int turn;
    public final int nodes;
    private Graph graph;
    public final LinkedList<LinkedList<Integer>> strategies;
    private LinkedList<LinkedList<Integer>> topology;
    private double selfish;


    /**
     * Constructor of Simulation
     *
     * @param n
     * @param l
     * @param w
     * @param h
     * @param m
     * @param s
     * @param t
     * @param sel
     */
    public Simulation(int n, int l, int w, int h, double[][]m, LinkedList<LinkedList<Integer>> s, LinkedList<LinkedList<Integer>> t, Double sel){
        this.nodes = n;
        this.turn = 1;
        this.limit = l;
        this.graph = new Graph(n,w,h,m);
        this.strategies = s;
        this.topology = t;
        this.selfish = sel;
    }


    /**
     * The method is called to initialize the graph.
     * @param w     an int that represents the weight of window
     * @param h     an int that represents the height of window
     */
    private void init_graph(int w, int h){
        Simulator.writing(Simulator.FILENAME,"Start initializing Graph\n");
        this.graph.init_nodes(this.nodes, this.strategies);
        this.graph.init_topology(w,h, this.topology);
        Simulator.writing(Simulator.FILENAME,"End initializing Graph\n");
        Simulator.writing(Simulator.FILENAME,"##############################\n"); // delimiter --> 30 #
    }

    /**
     * Method to run the simulation
     *
     * @param w     an int that represents the weight of the window
     * @param h     an int that represents the height of the window
     */
    public void run(int w, int h) {

        /* INITIALIZATION GRAPH */
        this.init_graph(w,h);
        double optimal_cost = this.graph.GetBestCost();

        /* GRAPHICAL INTERFACE */
        Topology tp = this.graph.GetTopology();
        if(Simulator.PRESENT){
            JViewer view = new JViewer(tp);
        }
        tp.start();

        /* INITIALIZATION OUTPUT */
        //JSONObject obj_init = new JSONObject();
        //obj_init.put("data", new JSONArray());
        //Simulator.writeJSON(Simulator.OUTPUT,obj_init);

        /* START SIMULATION */
        Simulator.writing(Simulator.FILENAME, "Start Simulation\n");

        /* THE SIMULATION KEEP GOING WHILE THE LIMITATION IS NOT REACH */
        while(this.turn <= this.limit){
            Simulator.writing(Simulator.FILENAME, "Start turn : " + this.turn + "\n");
            JSONObject obj = this.graph.turn(selfish);
            obj.put("turn", this.turn);

            /* COST */
            double cost = this.graph.calculateCost();
            Simulator.writing(Simulator.FILENAME, "Cost calculated : " + cost + "\n");
            obj.put("cost", cost);
            obj.put("optimal_cost", optimal_cost);

            /* UPDATE JSON */
            Simulator.writeJSON(Simulator.OUTPUT,obj);
            //Simulator.addJSONObjet(Simulator.OUTPUT,obj);
            //JSONArray array = (JSONArray) Simulator.DATA.get("data");
            //array.add(obj);
            //Simulator.DATA.put("data",array);

            /* MINIMISE OR MAXIMISE */
            boolean minimise = Simulator.STATUS.compareTo("COST") == 0 || Simulator.STATUS.compareTo("TIME") == 0;

            /* MINIMISE - STRATEGIES ARE RESPECTED AND BEST SOLUTION IS REACHED --> QUIT THE LOOP */
            if(minimise && this.graph.StrategiesRespected() && optimal_cost >= cost){
                break;
            }

            /* MAXIMISE - STRATEGIES ARE RESPECTED AND BEST SOLUTION IS REACHED --> QUIT THE LOOP */
            if(!minimise && this.graph.StrategiesRespected() && optimal_cost <= cost){
                break;
            }


            this.turn += 1;

            /* SLEEP TO VISUALIZE THE CHANGE OF TOPOLOGY */
            if(Simulator.PRESENT){
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        /* CONCLUSION */
        if(this.turn < this.limit){
            System.out.println("Finish : we have a convergence");
            Simulator.writing(Simulator.FILENAME, "Finish : we have a convergence" + "\n");
            //int diameter = Algorithms.getDiameter(this.graph.GetTopology());
            //Simulator.writing(Simulator.FILENAME, "Diameter of the finale topology : " + diameter + "\n");
        }
        else{
            System.out.println("Finish : we have reach the limit -> no convergence");
            Simulator.writing(Simulator.FILENAME, "Finish : we have reach the limit -> no convergence" + "\n");
        }
        //int diameter = Algorithms.getDiameter(this.graph.GetTopology());
        //System.out.println("Diameter of the finale topology : " + diameter);
        //Simulator.writing(Simulator.FILENAME, "Diameter of the finale topology : " + diameter + "\n");
        //Simulator.writeJSON(Simulator.OUTPUT,Simulator.DATA);

        System.exit(0);
    }
}
