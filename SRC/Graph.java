import io.jbotsim.core.Link;
import io.jbotsim.core.Topology;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.*;

public class Graph {
    private Topology topology;
    private LinkedList<Agent> nodes;
    private double[][] matrix_cost;
    private double best_cost;

    public Graph(int n, int w, int h, double[][] m){
        this.topology = new Topology(w,h);
        this.nodes = new LinkedList<>();
        this.matrix_cost = m;
        this.best_cost = this.calculateBestCost(this.matrix_cost, n);
    }

    /* ############################# INITIALIZATION ##################################### */

    /**
     * The method is called to initialize the agents of the simulation.
     * More precisely, it creates n Agent and adds them at the list
     * nodes (LinkedList<Agent>).
     * The method write the number of node in the graph into the logfile.
     * The method call the method display_matrix_logfile to display the
     * matrix of link's cost of the graph.
     * The method call the method display the strategy of each Agent into
     * the logfile
     * @param n     is the number of nodes in the graph/network
     */
    public void init_nodes(int n, LinkedList<LinkedList<Integer>> strategies){
        for(int i=0; i<n; i++){
            this.nodes.add(new Agent(strategies.get(i)));
        }
        Simulator.writing(Simulator.FILENAME, "number of Agents : " + n + "\n");
        display_strategies_logfile();
        display_matrix_logfile();
    }

    /**
     * Method is called to initialize the topology of the simulation
     * At the end, it calls the method display_topology_logfile() to
     * write the topology of the graph into the logfile.
     * @param w is the width of the window
     * @param h is the height of the window
     * @param t is the initial topology (topology from the configuration file)
     */
    public void init_topology(int w, int h, LinkedList<LinkedList<Integer>> t){
        Simulator.writing(Simulator.FILENAME, "Start initializing topology\n");

        LinkedList<Point> points = this.generate_position(this.nodes.size(), w, h); // generate random position for each node

        /* ATTRIBUTE A POSITION FOR EACH NODE */
        Simulator.writing(Simulator.FILENAME, "Position of each Agent :\n");
        for(int i=0; i<this.nodes.size(); i++){
            Point tmp = points.get(i);
            String description = " - " + this.nodes.get(i) + " = (" + tmp.x +" : "+ tmp.y + ")\n";
            Simulator.writing(Simulator.FILENAME, description);
            this.topology.addNode(tmp.x,tmp.y,this.nodes.get(i));
        }

        this.deleteLinks(); // delete links created by the library
        this.addLinks(t); // add links provided by the Simulation
        display_topology_logfile();
        Simulator.writing(Simulator.FILENAME, "End initializing topology\n");
    }

    /**
     * Method to generate n different positions (Point(x,y)) where
     * 0<=x<=w and 0<=y<=h. The positions is generated randomly.
     *
     * @param n is the number of nodes
     * @param w is the width of the window
     * @param h is the height of the window
     * @return LinkedList<Point>
     *
     * @see Point
     */
    private LinkedList<Point> generate_position(int n, int w, int h){
        LinkedList<Point> positions = new LinkedList<>();
        int count = 0;
        Random rand = new Random(21100187);
        while (count < n){
            double px = rand.nextDouble() * w;
            double py = rand.nextDouble() * h;
            boolean exist = false;

            /* to search if we use already this position */
            for(int i=0; i<positions.size() && exist==false; i++){
                Point tmp = positions.get(i);
                if(tmp.x == px && tmp.y == px){
                    exist = true;
                }
            }

            /* to verify if this position is not take */
            if(exist == false){
                positions.addLast(new Point(px,py));
                count += 1;
            }
        }
        return positions;
    }

    /**
     * Method to delete all links in the topology
     */
    public void deleteLinks(){
        List<Link> links = this.topology.getLinks();
        for (Link l : links){
            this.topology.removeLink(l);
        }
    }

    /**
     * Method to add links with the initial topology provided
     *
     * @param t
     */
    public void addLinks(LinkedList<LinkedList<Integer>> t){
        for(int i=0; i<t.size(); i++){
            for(int j=0; j<t.get(i).size(); j++){
                //System.out.println("adding link : ("+i+";"+t.get(i).get(j)+")\n");
                Link l = new Link(this.nodes.get(i), this.nodes.get(t.get(i).get(j)));
                this.topology.addLink(l);

                /* UPDATE AGENT'S LINKS */
                this.nodes.get(i).buyLink(l,this.matrix_cost[i][t.get(i).get(j)],this.matrix_cost);
                this.nodes.get(i).addLink(l);
                this.nodes.get(t.get(i).get(j)).addLink(l);
            }
        }
    }

    /* ############################# CALCULATE BEST COST ##################################### */

    /**
     * method to calculate the minimal cost (using an algorithm to construct a MST)
     * On calcule le cout minimal d'un chemin du graphe permettant a chaque noeud
     * d etre connecte aux autres noeuds (indirectement) -> Minimum Spanning Tree
     * Cela revient a trouver un MST et calculer le poid de ce MST
     *
     * @param matrix is a double[][] that represents the matrix of cost off all links
     * @param n is the number of nodes in the simulation
     * @return a double
     */
    /*
        function to calculate the minimal cost to respect the goal of each node
        is connected to all other nodes (using an algorithm to construct a MST)
        On calcule le cout minimal d'un chemin du graphe permettant a chaque noeud
        d etre connecte aux autres noeuds (indirectement) -> Minimum Spanning Tree
        Cela revient a trouver un MST et calculer le poid de ce MST
        */
    private double calculateBestCost(double[][] matrix, int n){

        /* CREATE A LIST OF LINKS VALUED */
        LinkedList<LinkValued> links = new LinkedList<>();
        for(int i=0; i<n; i++){
            for(int j=i+1; j<n; j++){
                links.add(new LinkValued(i,j,matrix[i][j]));
            }
        }

        /* SORT THE LIST IN DESCENDING ORDER --> MINIMISE (REMOVE LINK WITH HIGH WEIGHT) */
        if(Simulator.STATUS.compareTo("COST") == 0 || Simulator.STATUS.compareTo("TIME") == 0){
            Collections.sort(links, (a,b) -> b.compareTo(a));
        }

        /* SORT THE LIST IN ASCENDING ORDER --> MAXIMISE (REMOVE LINK WITH TINY WEIGHT) */
        else{
            Collections.sort(links, (a,b) -> a.compareTo(b));
        }

        /* REMOVE LINKS BELONG TO A CYCLE */
        int count = 0;
        while(count < links.size()){
            LinkValued l = links.get(count);
            if(belongCycle(count,(LinkedList<LinkValued>) links.clone())) {
                links.remove(count);
            }
            else {
                count++;
            }
        }

        /* CALCULATE OPTIMAL COST */
        double res;

        /* CALCULATE THE COST OR TIME */
        if(Simulator.STATUS.compareTo("COST") == 0 || Simulator.STATUS.compareTo("TIME") == 0){
            res = 0.0;
            for(LinkValued l : links){
                res += l.weight;
            }
        }

        /* CALCULATE THE EFFICIENT */
        else{
            res = 1.0;
            for(LinkValued l : links){
                res *= l.weight;
            }
        }
        
        Simulator.writing(Simulator.FILENAME, "Best cost : " + res + "\n");
        return res;
    }

    /**
     * Method to determinate if the link l belongs to a cycle/loop
     * in links it is meaning that if the two nodes of the link l
     * are reachable/connected using the links in links without l
     * so the link l belongs to a cycle.
     *
     * @param index is the index of the link wanted (that we want check)
     * @param links is a list of all links
     * @return a boolean
     */

    public boolean belongCycle(int index, LinkedList<LinkValued> links){

        /* REMOVE THE LINK */
        LinkValued l = links.remove(index);
        int src = l.node1;
        int dst = l.node2;

        /* DEPTH FIRST SEARCH */
        LinkedList<Integer> nodes_visited = new LinkedList<>();
        depth_first_search(src,nodes_visited,links);

        return nodes_visited.contains(dst);
    }

    /**
     * This method is the Depth First Search algorithm.
     * This is a recursive method/algorithm.
     *
     * @param src is the node source
     * @param node_visited is a list of nodes visited
     * @param links is the list of all links
     */
    private void depth_first_search(int src, LinkedList<Integer> node_visited,LinkedList<LinkValued> links){
        node_visited.addLast(src);

        /* GET THE NEIGHBORS OF SRC */
        LinkedList<Integer> neighbors = new LinkedList<>();
        for(LinkValued l : links){
            if(l.node1 == src){
                neighbors.addLast(l.node2);
            }
            if(l.node2 == src){
                neighbors.addLast(l.node1);
            }
        }

        /* BROWSE NEIGHBORS OF SRC */
        for(int nei : neighbors){
            if(node_visited.contains(nei) == false){
                depth_first_search(nei,node_visited,links);
            }
        }
    }

    /* ############################# MANAGE TURN ##################################### */

    /*
    function that returns true if the strategy of all player/agent
    is respected else returns false
     */
    public boolean StrategiesRespected(){
        for(Agent a : this.nodes){
            if(a.strategyRespected() == false){
                return false;
            }
        }
        return true;
    }

    /**
     * Method to manage a turn in the simulation
     *
     * @param selfish
     * @return
     */
    public JSONObject turn(double selfish){

        /* JSON OBJECT TURN */
        JSONObject turn = new JSONObject();

        /* CREATE THE PLAYERS'S ORDER */
        LinkedList<Integer> order = new LinkedList<>();
        for(int i=0; i<this.nodes.size(); i++){
            order.add(i);
        }
        Collections.shuffle(order);
        display_order_play_logfile(order);

        /* JSON ORDER */
        JSONArray array_order = this.order_play_to_JSON(order);
        turn.put("play_order",array_order);

        /* JSON AGENTS */
        JSONArray array_agents = new JSONArray();

        /* AGENTS PLAY BY FOLLOWING THE ORDER */
        for(int i=0; i<this.nodes.size(); i++){
            int ident = order.get(i);
            for(int j=0; j<this.nodes.size(); j++){
                if(this.nodes.get(j).agent_id == ident){
                    this.nodes.get(j).play(this.nodes, this.matrix_cost, selfish);
                    JSONObject obj = this.nodes.get(j).Agent_to_JSON();
                    array_agents.add(obj);
                }
            }
        }
        turn.put("agents", array_agents);

        /* TOPOLOGY */
        display_topology_logfile();
        JSONArray topo = this.topoology_to_JSON();
        turn.put("topology", topo);

        return turn;
    }

    /**
     * Method to calculate the current cost of the graph.
     * The way to calculate the current cost change in terms of
     * the value of STATUS. Indeed, if STATUS iS : COST or TIME then
     * the cost is additive else the cost is multiplicative
     *
     * @return a double
     */
    public double calculateCost(){
        List<Link> topo_links = this.topology.getLinks();
        double res = 0.0;

        /* BROWSE LINKS */
        for(Link l : topo_links) {
            Agent src = (Agent) l.source;
            Agent dst = (Agent) l.destination;
            double cost = this.matrix_cost[src.agent_id][dst.agent_id];

            /* CALCULATE THE COST OR TIME */
            if(Simulator.STATUS.compareTo("COST") == 0 || Simulator.STATUS.compareTo("TIME") == 0){
                res += cost;
            }

            /* CALCULATE THE EFFICIENT */
            else{
                res *= cost;
            }
        }
        return res;
    }

    /* ########################## DISPLAY LOGFILE ################################ */

    /**
     * Method to write the topology into the logfile
     */
    private void display_topology_logfile(){
        Simulator.writing(Simulator.FILENAME, "initial topology : \n");
        List<Link> topo_links = this.topology.getLinks();
        for(Link l : topo_links){
            Agent src = (Agent) l.source;
            Agent dst = (Agent) l.destination;
            Simulator.writing(Simulator.FILENAME, src + " <--> " + dst + "\n");
        }
    }

    /**
     * Method to write the matrix of link's cost of the graph into the
     * logfile
     */
    private void display_matrix_logfile(){
        Simulator.writing(Simulator.FILENAME, "matrix of link's cost : \n");
        for(int i = 0; i < this.nodes.size(); i++){
            String s = "[ ";
            for(int j = 0; j < this.nodes.size(); j++){
                s += this.matrix_cost[i][j];
                if(j+1 < this.nodes.size()){
                    s += " ; ";
                }
            }
            s += " ]\n";
            Simulator.writing(Simulator.FILENAME, s);
        }
    }

    /**
     * Method to write the strategy of each player in the logfile
     */
    private void display_strategies_logfile(){
        Simulator.writing(Simulator.FILENAME, "strategy of each Agent :\n");
        for(int i=0; i < this.nodes.size(); i++){
            String s = " - " + this.nodes.get(i) + "'strategy : {";
            String strat = this.nodes.get(i).display_strategy_logfile();
            s += strat + "}\n";
            Simulator.writing(Simulator.FILENAME, s);
        }
    }

    /**
     * Method to write the order of play into the logfile
     *
     * @param l represents the order of play
     */
    private void display_order_play_logfile(LinkedList<Integer> l){
        Simulator.writing(Simulator.FILENAME, "play's order :\n");
        for(int i=0; i<l.size(); i++){
            Simulator.writing(Simulator.FILENAME, " - Agent " + l.get(i) + "\n");
        }
    }

    /* ########################## MANAGE JSON ################################ */

    /**
     * Method to create a JSONArray that represents the Agent's order
     * to play this turn.
     *
     * @param order is a list which contain the Agent's order to play
     * @return JSONArray
     */
    private JSONArray order_play_to_JSON(LinkedList<Integer> order){
        JSONArray res = new JSONArray();
        for(int ele : order){
            res.add(ele);
        }
        return res;
    }

    /**
     * Method to create a JSONArray that represents the topology
     * of the graph in this turn.
     *
     * @return JSONArray
     */
    private JSONArray topoology_to_JSON(){
        JSONArray res = new JSONArray();
        List<Link> topo_links = this.topology.getLinks();
        for(Link l : topo_links){
            Agent src = (Agent) l.source;
            Agent dst = (Agent) l.destination;

            JSONArray link = new JSONArray();
            link.add(src.agent_id);
            link.add(dst.agent_id);
            res.add(link);
        }
        return res;
    }

    /* ############################# GETTERS ##################################### */

    public Topology GetTopology(){ return this.topology;}

    public double GetBestCost(){ return this.best_cost;}

    /* ##################### PRIVATE CLASS POINT ################################# */

    private class Point{
        public final double x;
        public final double y;

        public Point(double px, double py){
            this.x = px;
            this.y = py;
        }
    }

    /* ##################### PRIVATE CLASS LINKVALUED ############################# */

    private class LinkValued{
        int node1;
        int node2;
        double weight;

        public LinkValued(int n1, int n2, double w){
            this.node1 = n1;
            this.node2 = n2;
            this.weight = w;
        }

        public int compareTo(LinkValued l){
            if(this.weight > l.weight){
                return 1;
            }
            else if(this.weight == l.weight){
                return 0;
            }
            else {
                return -1;
            }
        }

        public String toString(){
            return "("+this.node1+"-->"+this.node2+") ["+this.weight+"]";
        }
    }
}
