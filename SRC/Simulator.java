import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import org.json.simple.*;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Simulator {

    /* DEFAULT VARIABLES */
    public static int NB_NODES = 1;
    public static int LIMIT = 1;
    public static double[][] MATRIX;
    public static LinkedList<LinkedList<Integer>> STRATEGY ;
    public static LinkedList<LinkedList<Integer>> TOPOLOGY ;
    public static double SELFISH = 0.5; // 0 --> agent not selfish and 1 --> agent selfish
    public static String STATUS; // COST or EFFICIENT or TIME

    /* FILE'S VARIABLE */
    public static String FILENAME = null; //logfile
    public static String OUTPUT = null; //raw date --> json
    //public static JSONObject DATA = new JSONObject();

    /* WINDOW VARIABLES */
    public static int WIDTH = 1000;
    public static int HEIGHT = 1000;
    public static boolean PRESENT;

    /*####################################### MANAGE SIMULATOR ########################################### */

    /**
     * Show information to use correctly the Simulator
     *
     * @return void
     */
    public static void help(){}

    /**
     * Returns a Simulation created by the parameters provided in the
     * configuration file. This method create the Simulation
     *
     * @param nodes represents the number of nodes in the simulation
     * @param limit represents the limitation of number of turns in the simulation
     * @param width represents the width of the window
     * @param height represents the height of the window
     * @param matrix represents the matrix of cost of all possible links in the Graph
     * @param strategy represents the strategy of each Agent in the simulation
     * @param topology represents the topology of the Graph in the simulation
     * @param selfish represents the selfishness rate of the Agents in the simulation
     *
     * @return a Simulation
     * @see Simulation
     */
    public static Simulation create_simulation(int nodes, int limit, int width, int height, double[][] matrix, LinkedList<LinkedList<Integer>> strategy, LinkedList<LinkedList<Integer>> topology, Double selfish){
        return new Simulation(nodes, limit, width, height, matrix, strategy, topology, selfish);
    }

    /*################################ CHECK PARAMETERS'S VALIDITY ##################################### */

    /**
     *
     * @return a boolean
     */
    public static boolean check_parameters(){

        /* NB_NODES */
        if(Simulator.NB_NODES <= 0){
            return false;
        }
        /* LIMIT */
        if(Simulator.LIMIT <= 0){
            return false;
        }
        /* SELFISH */
        if(Simulator.SELFISH < 0 || Simulator.SELFISH > 1){
            return false;
        }
        /* STATUS */
        if(Simulator.STATUS == null || (Simulator.STATUS.compareTo("COST") != 0 && Simulator.STATUS.compareTo("EFFICIENT") != 0 && Simulator.STATUS.compareTo("TIME") != 0 )){
            return false;
        }

        /* WIDTH */
        if(Simulator.WIDTH <= 0 ){
            return false;
        }

        /* HEIGHT */
        if(Simulator.HEIGHT <= 0 ){
            return false;
        }

        /* STRATEGY */
        if ( NB_NODES > STRATEGY.size()){
            return false;
        }
        for( int i=0; i<STRATEGY.size(); i++){
            for (int j=0; j<STRATEGY.get(i).size();j++){
                if(STRATEGY.get(i).get(j) == null || STRATEGY.get(i).get(j) < 0 || STRATEGY.get(i).get(j) == i || STRATEGY.get(i).get(j) >= NB_NODES ){
                    return false;
                }
            }
            if ( NB_NODES <= STRATEGY.get(i).size()){
                return false;
            }
        }

        /* MATRIX */
        for(int i=0; i<MATRIX.length; i++){
            for(int j=0; j<MATRIX[i].length; j++){
                if(MATRIX[i][j] != -1.0 && i==j ){
                    return false;
                }
                if(i != j && (MATRIX[i][j] == -1.0 || MATRIX[i][j] != MATRIX[j][i] || MATRIX[i][j] < 0)){
                    return false;
                }
            }
        }

        /* TOPOLOGY */
        boolean[][] topo = new boolean[NB_NODES][NB_NODES];
        for(int i=0; i<NB_NODES; i++){
            for(int j=0; j<NB_NODES; j++){
                topo[i][j] = false;
            }
        }

        for(int i=0; i<TOPOLOGY.size(); i++){
            for(int j=0; j<TOPOLOGY.get(i).size(); j++){
                if(topo[i][TOPOLOGY.get(i).get(j)] == true || topo[TOPOLOGY.get(i).get(j)][i] == true || i == TOPOLOGY.get(i).get(j) ){
                    return false;
                }
                else{
                    topo[i][TOPOLOGY.get(i).get(j)] = true;
                    topo[TOPOLOGY.get(i).get(j)][i] = true;
                }
            }
        }

        return true;
    }

    /*####################################### MANAGE LOGFILE ########################################### */

    /**
     * Returns a String that represents the log filename.
     * This methods always return an unique name because the name
     * is composed by the current local datetime.
     * The format of the name is : log__dd_MM_yyyy__HH_mm_ss
     *
     * @return the name of the new log file
     * @see String
     */
    public static String log(){

        /* CREATE THE FILENAME */
        LocalDateTime date = LocalDateTime.now();
        DateTimeFormatter formatDate = DateTimeFormatter.ofPattern("dd_MM_yyyy__HH_mm_ss");
        String idDate = date.format(formatDate); // current datetime formatted
        String filename = "log__" + idDate;

        /* CREATE THE LOGFILE */
        try {
            File logfile = new File(filename);
            if (logfile.createNewFile()) {
                System.out.println("File created: " + logfile.getName());
            } else {
                System.out.println("File already exists.");
            }
        } catch (IOException e) {
            System.out.println("An error occurred during the creation of the logfile.");
            e.printStackTrace();
            return null;
        }
        return filename;
    }

    /**
     * Writes the String buffer in the file named filename
     *
     * @param filename is the name of file
     * @param buffer is the string that must be write in the file
     */
    public static void writing(String filename, String buffer){
        try {
            FileWriter writer = new FileWriter(filename, true);
            writer.write(buffer);
            writer.close();
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    /*####################################### CONFIGURATION FILE ########################################### */

    /**
     * Method to get the matrix of cost of all possible links in the graph from
     * a JSONArray (2 dimensions) (get information from the configuration file).
     * The matrix is represented in a JSONArray and this method copy/get the matrix
     * into a double[][].
     *
     * @param array is a JSONArray that represents the matrix
     * @return a double[][]
     */
    public static double[][] getMatrix(JSONArray array){
        double[][] res = new double[NB_NODES][NB_NODES];
        int i = 0;
        for(Object ele : array){
            int j = 0;
            LinkedList<Double> l = new LinkedList<>();
            for(Object x : (JSONArray) ele){
                if(x == null){ // it's meaning that i == j
                    res[i][j] = -1.0;
                }
                else{
                    res[i][j] = (double) x;
                }
                j++;
            }
            i++;
        }
        return res;
    }

    /**
     * Method to get the strategy of each Agent from a JSONArray (2 dimensions)
     * (get information from the configuration file). The strategies are represented
     * in a JSONArray and this method copy/get the strategies into a LinkedList<LinkedList<Integer>>.
     * Each element of the JSONArray correspond to a strategy so the element i correspond to the strategy
     * of the Agent i.
     *
     * @param array is a JSONArray that represents the strategy of each Agent
     * @return a LinkedList<LinkedList<Integer>>
     *
     * @see LinkedList
     * @see Integer
     * @see JSONArray
     */
    public static LinkedList<LinkedList<Integer>> getStrategy(JSONArray array){

        LinkedList<LinkedList<Integer>> res = new LinkedList<>();
        for(Object ele : array){ // to get a strategy
            LinkedList<Integer> l = new LinkedList<>();
            for(Object x : (JSONArray) ele){ // to get an element of the strategy
                long y = (long) x;
                l.addLast((int) y);
            }
            res.addLast(l);
        }
        return res;
    }

    /**
     * Method to get the topology of the graph from a JSONArray (list of JSONObject)
     * (get information from the configuration file). The topology is represented
     * in a JSONArray and this method copy/get the links (topology) into a LinkedList<LinkedList<Integer>>.
     *
     * @param array is a JSONArray that represents the topology of the graph
     *              (specially a list of JSONObject which contains :
     *              - id  : the id of buyer --> it's meaning it is him
     *                      who buy the links
     *              - neighbors : a JSONArray that represents the id of
     *                            its neighbors
     *              )
     * @return a LinkedList<LinkedList<Integer>>
     *
     * @see LinkedList
     * @see Integer
     * @see JSONArray
     */
    public static LinkedList<LinkedList<Integer>> getTopology(JSONArray array){

        /* CREATE THE LIST TO CONTAIN THE TOPOLOGY */
        LinkedList<LinkedList<Integer>> res = new LinkedList<>();
        for(int i=0; i<Simulator.NB_NODES; i++){
            res.addLast(new LinkedList<>());
        }

        /* ADDING THE LINKS AT THE GOOD POSITIONS */
        for(Object ele : array){
            JSONObject element = (JSONObject) ele;
            long id = (long) element.get("id"); // the buyer
            JSONArray tab = (JSONArray) element.get("neighbors");

            for(Object x : tab){
                long y = (long) x;
                res.get((int) id).addLast((int) y);
            }
        }

        return res;
    }

    /**
     * Method to read the configuration file and get the data into a format choose.
     * This method read file.json.
     *
     * @param filename is a string representing the filename of the configuration file.
     *
     * @see JSONParser
     * @see JSONObject
     * @see FileReader
     */
    public static void read_configuration_file(String filename){

        /* PARSER */
        JSONParser jsonParser = new JSONParser();

        try (FileReader reader = new FileReader(filename))
        {
            /* READ THE CONFIGURATION FILE */
            Object configuration = jsonParser.parse(reader);
            JSONObject configurationFile = (JSONObject) configuration;

            /* GET DATA */
            long nodes = (long) configurationFile.get("number_nodes");
            Simulator.NB_NODES = (int) nodes;
            long limit = (long) configurationFile.get("limit_turns");
            Simulator.LIMIT = (int) limit;

            /* IF GRAPHICAL INTERFACE IS ACTIVE */
            if(PRESENT){
                long w = (long) configurationFile.get("width");
                Simulator.WIDTH = (int) w;
                long h = (long) configurationFile.get("height");
                Simulator.HEIGHT = (int) h;
            }
            Simulator.MATRIX = Simulator.getMatrix((JSONArray) configurationFile.get("matrix"));
            Simulator.STRATEGY = Simulator.getStrategy((JSONArray) configurationFile.get("strategies"));
            Simulator.TOPOLOGY = Simulator.getTopology((JSONArray) configurationFile.get("topology"));
            Simulator.SELFISH = (double)configurationFile.get("selfish");
            Simulator.STATUS = (String) configurationFile.get("status");

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    /*################################### JSON OUTPUT ######################################### */

    /**
     * Returns a String that represents the file.json.
     * This methods always return an unique name because the name
     * is composed by the current local datetime.
     * The format of the name is : rawData__dd_MM_yyyy__HH_mm_ss.json
     *
     * @return the name of the new file.json
     * @see String
     */
    public static String createFileJSON(){

        /* CREATE THE FILENAME */
        LocalDateTime date = LocalDateTime.now();
        DateTimeFormatter formatDate = DateTimeFormatter.ofPattern("dd_MM_yyyy__HH_mm_ss");
        String idDate = date.format(formatDate); // current datetime formatted
        String filename = "rawData__" + idDate + ".json";

        /* CREATE THE LOGFILE */
        try {
            File logfile = new File(filename);
            if (logfile.createNewFile()) {
                System.out.println("File created: " + logfile.getName());
            } else {
                System.out.println("File already exists.");
            }
        } catch (IOException e) {
            System.out.println("An error occurred during the creation of the logfile.");
            e.printStackTrace();
            return null;
        }

        //DATA.put("data",new JSONArray());

        return filename;
    }

    /**
     * Returns a String that represents the file.json.
     * This methods always return an unique name because the name
     * is composed by the finaname_base given in parameter.
     *
     * @return the name of the new file.json
     * @see String
     */
    public static String createFileJSON(String filename_base){

        /* CREATE THE FILENAME */
        String filename = filename_base + ".json";

        /* CREATE THE LOGFILE */
        try {
            File logfile = new File(filename);
            if (logfile.createNewFile()) {
                System.out.println("File created: " + logfile.getName());
            } else {
                System.out.println("File already exists.");
            }
        } catch (IOException e) {
            System.out.println("An error occurred during the creation of the logfile.");
            e.printStackTrace();
            return null;
        }

        //DATA.put("data",new JSONArray());

        return filename;
    }
    /**
     * Writes the JSONObject in the file named filename
     *
     * @param filename is the name of file
     * @param obj is JSONObject that must be write in the file
     */
    public static void writeJSON(String filename, JSONObject obj){
        FileWriter writer;
        try {
            writer = new FileWriter(filename, true);
            writer.write(obj.toJSONString() + "\n");
            writer.close();
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    /**
     * Method to update the JSONObject from the output file.
     * In a first time, we get the JSONObject from the output file,
     * then we add a new JSONObject in the JSONArray data provided
     * by the JSONObject from the output file. Finally, we write the
     * JSONObject updated into the output file.
     *
     * @param filename is the name of file
     * @param obj is JSONObject that must add to the JSONArray data
     */
    public static void addJSONObjet(String filename, JSONObject obj){

        JSONParser jsonParser = new JSONParser();
        try (FileReader reader = new FileReader(filename)) {

            /* READ THE OUTPUT FILE */
            Object out = jsonParser.parse(reader);

            /* GET DATA */
            JSONObject outputFile = (JSONObject) out;
            JSONArray array = (JSONArray) outputFile.get("data");

            /* ADDING A NEW JSONOBJECT */
            array.add(obj);

            /* UPDATE THE OUTPUT FILE */
            FileWriter writer;
            writer = new FileWriter(filename);
            writer.write(outputFile.toJSONString() + "\n");
            writer.close();
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    /*####################################### MAIN ########################################### */

    public static void main(String[] args){

        if(args.length >0) {
            for(String s:args) {
                String[] option = s.split("=");
                if(option[0].equals("config")) {
                    System.out.println("Reading configuration file");
                    Simulator.read_configuration_file(option[1]);
                }
                if(option[0].equals("output")) {
                    System.out.println("Creating requested output file");
                    OUTPUT = createFileJSON(option[1]);        
                }
                if(option[0].equals("gui")) {
                    if(option[1].equals("TRUE")) {
                        System.out.println("GUI option enabled");
                        PRESENT = true;
                    }
                    else {
                        System.out.println("GUI option disabled");
                        PRESENT = false;
                    }
                }
            }
            if(FILENAME == null) {
                FILENAME = log();
                System.out.println("Creating log file");
            }
            if(OUTPUT == null) {
                System.out.println("No output file was given creating one...");
                OUTPUT = createFileJSON();
            }
            if(FILENAME != null && OUTPUT != null  && check_parameters()){
                System.out.println("Launching simulation");
                Simulation simulation = create_simulation(NB_NODES,LIMIT,WIDTH,HEIGHT, MATRIX, STRATEGY, TOPOLOGY, SELFISH);
                simulation.run((int) WIDTH,(int) HEIGHT);
            }
        }
        else{
            System.out.println("At least one argument must be provided");
        }
    }
}