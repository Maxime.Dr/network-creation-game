import json

def convert(infile, outfile):
    with open(infile) as in_f:
        data = in_f.read()
        dict_json = dict()
        dict_json["data"] = list()
        list_turns = data.split("\n")
        for ele in list_turns :
            if(len(ele)>1) :
                turn = json.loads(ele)
                dict_json["data"].append(turn)
        json_string = json.dumps(dict_json, indent = 4)
        with open(outfile, "w") as out_f:
            out_f.write(json_string)