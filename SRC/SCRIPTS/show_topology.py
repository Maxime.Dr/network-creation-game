from importlib import import_module
import json
import sys
from re import T
import numpy
import pandas
from numpy import arange
from matplotlib import pyplot as matplotlib
import networkx as nx

"""
fonction retournant une liste contenant les informations de chaque tour
de la simulation.
"""
def readingJSON(filename) :
    with open(filename) as json_file:
        data = json.load(json_file)
        return data["data"] 

"""
fonction retournant un dictionnaire contenant les informations de la 
topologie initiale
"""
def readingJSON_config(filename) :
    with open(filename) as json_file:
        data = json.load(json_file)
        return data["topology"] 

    ######################### INITIAL TOPOLOGY #########################
def generate_png(configuration,name):
    namefile = configuration
    ele = readingJSON_config(namefile)
    # ANALYSE OF THE TOPOLOGY
    l_from = list()
    l_dest = list()
    for d in ele :
        index = d["id"]
        voisins = d["neighbors"]
        for v in voisins : 
            l_from.append(str(index))
            l_dest.append(str(v))
    
    dico = {
        'from' : l_from,
        'dest' : l_dest
    }
    df = pandas.DataFrame(dico)
    G = nx.from_pandas_edgelist(df,'from','dest')
    nx.draw(G, with_labels=True, node_size=100, alpha=1, linewidths=10)
    matplotlib.savefig(configuration + "_initial_topology.png")   
    matplotlib.clf()
    ######################### FINAL TOPOLOGY ###########################
            
 
    namefile = name
    ele = readingJSON(namefile)

    #nx.clear()
    # ANALYSE LAST TURN OF THE SIMULATION
    index_last = len(ele)-1
    last_turn = ele[index_last]
    topo = last_turn["topology"]
    l_from = list()
    l_dest = list()
    for tup in topo :   
        l_from.append(str(tup[0]))
        l_dest.append(str(tup[1]))
    dico = {
        'from' : l_from,
        'dest' : l_dest
    }
    df = pandas.DataFrame(dico)
    G = nx.from_pandas_edgelist(df,'from','dest')
    nx.draw(G, with_labels=True, node_size=100, alpha=1, linewidths=10)
    matplotlib.savefig(name + "_final_topology.png")   

# Computes the social cost of graph, given parameter alpha
# the social cost is equal to the sum of distances between nodes (communication cost),  
# plus alpha times the number of bought edges (construction cost) 
def social_cost(alpha,graph):
    if (nx.is_connected(graph)):
        spl = dict(nx.all_pairs_shortest_path_length(graph))
        cost = 0
        for n in spl:
            for p in spl:
                try:
                    cost += spl[n][p]
                except KeyError:
                    print(f"no distance found between n:{n}, p:{p}")
        cost += nx.number_of_edges(graph)*alpha
        return cost
    return -1

def is_connected(graph):
    return nx.is_connected(graph)
    
# Returns true is the graph is a tree
# A tree is a connected graph with n-1 edges
def is_tree(graph):
    if(nx.is_connected(graph)):
        if(nx.number_of_edges(graph) == nx.number_of_nodes(graph)-1):
            return True
    return False

# Returns true is the graph is a forest
# A forest is a graph with n-m edges, where m is the number of connected components
def is_forest(graph):
    m = nx.number_connected_components(graph)
    if(nx.number_of_edges(graph) == nx.number_of_nodes(graph)-m):
        return True
    return False

# for each configuration in the history, outputs the social cost of the current configuration, 
# along with whether the configuration is a tree
def analyse_history(filename):
    all = readingJSON(filename)
    for element in all:
        topo = element["topology"]
        l_from = list()
        l_dest = list()
        for tup in topo :   
            l_from.append(str(tup[0]))
            l_dest.append(str(tup[1]))
        dico = {
            'from' : l_from,
            'dest' : l_dest
        }
        df = pandas.DataFrame(dico)
        G = nx.from_pandas_edgelist(df,'from','dest')
        print(f"Social cost:{social_cost(1,G)}, is a tree:{is_tree(G)}, is a forest:{is_forest(G)}") 

if __name__ == "__main__" :
    if(len(sys.argv)==3):
        configuration = sys.argv[1]
        output = sys.argv[2]
        generate_png(configuration,output)
        analyse_history(output)
    else:
        print(f"syntax:{sys.argv[0]} configuration.json output.json")