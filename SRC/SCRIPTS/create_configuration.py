import sys
import json
import random
import time
import subprocess
import utils
import show_topology
import ramdisk

def compute_metrics(outcome):
    res = dict()
    res["time"]= outcome['turn']
    res["cost"]=0.0
    res["changes"]=0
    for e in outcome['agents']:
        res["cost"] += e["weight"]
        res["changes"] += e["number_changes"]
    return res

def run_simulations_vary_selfish(config,from_selfish,to_selfish,step_selfish,output_prefix,nb):
    dict_config = load_json(config)
    res = dict()
    i = from_selfish
    while i < to_selfish:
        dict_config['selfish'] = i
        save_config(dict_config)
        res[i] = run_simulations_different_config(config,output_prefix+"_selfish"+str(i),nb)
        i += step_selfish
    return res

def run_simulations_vary_size(config,from_size,to_size,step_size,output_prefix,nb):
    dict_config = load_json(config)
    res = dict()
    i = from_size
    while i < to_size:
        dict_config['number_nodes'] = i
        dict_config["matrix"] = create_matrix(i, 10)
        dict_config["strategies"] = create_strategy(i)
        dict_config["topology"] = create_topology(i)
        save_config(dict_config)
        res[i]=run_simulations_different_config(config,output_prefix+"_selfish"+str(i),nb)
        i += step_size
    return res

def run_simulations_different_config(config,output_prefix,nb):
    res = []
    for i in range(nb):
        dict_config = load_json(config)
        nbr = dict_config['number_nodes']
        dict_config["matrix"] = create_matrix(nbr, 10)
        dict_config["strategies"] = create_strategy(nbr)
        dict_config["topology"] = create_topology(nbr)
        save_config(dict_config)
        run_one_simulation(config,output_prefix+"_"+str(i))
        utils.convert(output_prefix+"_"+str(i)+'.json',output_prefix+"_c_"+str(i)+'.json')
        dict_outcome = load_json(output_prefix+"_c_"+str(i)+'.json')
        if(len(dict_outcome['data'])>0):
            last_outcome = dict_outcome['data'][-1]
            res.append(compute_metrics(last_outcome))
    return res

def run_simulations_same_config(config,output_prefix,nb):
    res = []
    for i in range(nb):
        run_one_simulation(config,output_prefix+"_"+str(i))
        utils.convert(output_prefix+"_"+str(i)+'.json',output_prefix+"_c_"+str(i)+'.json')
        dict_outcome = load_json(output_prefix+"_c_"+str(i)+'.json')
        if(len(dict_outcome['data'])>0):
            last_outcome = dict_outcome['data'][-1]
            res.append(compute_metrics(last_outcome))
    return res

def run_one_simulation(config,output):
    jar_location = "../../network-creation-game.jar"
    print(['java','-jar',jar_location,"config="+str(config),"output="+str(output)])
    subprocess.run(['java','-jar',jar_location,"config="+str(config),"output="+str(output)])

"""
    Creates the configuration dictionary with default parameters
"""
def default_config():
    d = dict()
    d["number_nodes"] = 100
    d["limit_turns"] = 10000
    d["selfish"] = 0.5
    d["status"] = "COST"
    d["width"] = 500
    d["heigth"] = 500
    d["filename"] = "config"
    d["matrix"] = create_matrix(100, 10)
    d["strategies"] = create_strategy(100)
    d["topology"] = create_topology(100)
    return d

"""
    Creates the configuration dictionary from the configuration parameters
"""
def create_config(nodes, limit, selfish, status, width, heigth, max_value, filename):
    d = default_config()
    d["number_nodes"] = nodes
    d["limit_turns"] = limit
    d["selfish"] = selfish
    d["status"] = status
    d["width"] = width
    d["heigth"] = heigth
    d["filename"] = filename
    d["matrix"] = create_matrix(nodes, max_value)
    d["strategies"] = create_strategy(nodes)
    d["topology"] = create_topology(nodes)
    return d

"""
    Creates the configuration dictionary from a list of strings in the form "param=value" (obtained e.g. from the command line)
"""
def create_config(list):
    d = default_config()
    operations = {
        "number_nodes": lambda x: int(x),
        "limit_turns": lambda x: int(x),
        "selfish": lambda x: float(x),
        "status": lambda x: x,
        "width": lambda x: int(x),
        "height": lambda x: int(x),
        "max_value": lambda x: int(x),
        "filename": lambda x: x
    }
    for s in list:
        [l,v] = s.split("=")
        d[l] = operations[l](v)
    d["matrix"] = create_matrix(d["number_nodes"], d["max_value"])
    d["strategies"] = create_strategy(d["number_nodes"])
    d["topology"] = create_topology(d["number_nodes"])
    return d

"""
    Creates the .json file from the config dictionary (using its "filename" parameter)
"""
def save_config(config): 
    with open(config["filename"] + ".json", 'w') as f :
        f.write(json.dumps(config, indent=4))

"""
    Loads the config dictionary from a file
"""
def load_json(file): 
    res = dict()
    with open(file,'r') as f:
        res=json.load(f)
    return res

"""
def create_matrix(nodes, limit) :
    ** nodes : is a int that represents the number of nodes
    ** limit : is a float that represents the maximal value 

    Method to create a matrix with values limited by the value
    limit. The value of the elements in the matrix are :
    0 <= value < limit and null on the diagonal.
    The matrix created is symetric.

    Returns a list(list(float))
"""
def create_matrix(nodes, limit) :
    random.seed(time.time())
    res = []

    # CREATE THE MATRIX 
    for i in range(nodes) :
        tab = []
        for j in range(nodes) :
            if i == j :
                tab.append(None)
            if j < i :
                r = random.random() * limit
                tab.append(r)
        res.append(tab)

    # COMPLETE THE MATRIX TO OBTAIN A SYMETRIC MATRIX
    for i in range(nodes) :
        for j in range(nodes) :
            if i < j :
                r = res[j][i]
                res[i].append(r)

    return res

"""
def create_strategy(nodes) :
    ** nodes : is a int that represents the number of nodes

    Method to create the strategy for each node.
    In a first time, we store the all id possible for
    the strategies. Secondly, we create a strategy for 
    each nodes randomly (by shuffling the list of all index)
    and we add the id if it is not the same of the node.

    Returns a list(list(int))
"""
def create_strategy(nodes) :
    res = list()
    index = [i for i in range(nodes)] # store the id of all nodes

    # CREATION STRATEGY FOR EACH NODES
    for i in range(nodes) : 
        r = random.randrange(nodes) # choose randomly the size of strategy
        random.shuffle(index) 
        strat = list() # store the strategy
        
        # GET THE STRATEGY 
        for j in range(r) :
            if index[j] != i : # avoid to add himself into its strategy
                strat.append(index[j])
        res.append(strat)

    return res

"""
def create_topology(nodes) :
    ** nodes :  is a int that represents the number of nodes

    Method to create randomly a topology. 
    In a first time, we create a certain number of 
    egdes/links randomly. Then we store theses links
    into a dictionnary. Finally, we convert the dictionnary
    into a list of dictionnary in order to have the right 
    format for the configuration file.

    Returns a list of dictionnary
"""   
def create_topology(nodes) :
    res = list()
    edges = nodes * (nodes-1) // 2 # maximal number of edges into a graph with nodes nodes
    link_used = set()

    # CREATION EDGES RANDOMLY
    r = random.randrange(edges+1)
    while len(link_used) < r : 
        n1 = random.randrange(nodes)
        n2 = random.randrange(nodes)
        
        if n1 != n2 and (n1,n2) not in link_used and (n2,n1) not in link_used :
            link_used.add((n1,n2))
    
    # PUT AND ORDER EDGES INTO A DICTIONARY
    d = dict()
    for ele in link_used :
        n1 = ele[0]
        n2 = ele[1]
        if n1 in d.keys() :
            d[n1].append(n2)
        else :
            d[n1] = [n2]

    # CREATE TOPOLOGY --> CONFIGURATION FILE JSON
    for k,v in d.items() :
        dico = dict()
        dico["id"] = k
        dico["neighbors"] = v
        res.append(dico)

    return res

if __name__ == "__main__":
    save_config(create_config(sys.argv[1:]))
    outcome = run_simulations_vary_selfish("toto.json",0.1,0.9,0.1,"output",50)
    print(outcome)
