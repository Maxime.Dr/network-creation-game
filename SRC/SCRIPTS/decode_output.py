import sys
import utils

if __name__ == "__main__" :
    if(len(sys.argv)==3):
        infile = sys.argv[1]
        outfile = sys.argv[2]
        utils.convert(infile,outfile)
    else:
        print(f"syntax:{sys.argv[0]} infile.json outfile.json")